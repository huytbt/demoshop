<?php

class ProductController extends Controller
{
    public function actionIndex($cat_id = null)
    {
        $categories = Category::model()->findAll();
        $this->render('index', array(
            'categories' => $categories,
        ));
    }
}
